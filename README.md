# SBF Data Record Filter

Extract all data records of a specific type (block number) from a Septentrio Binary Format data file. The records are written to standard output in [newline-delimited JSON](http://ndjson.org) format.

## Installation

Download the appropriate binary from the Downloads section of this repository.

### Install from source

The [Go compiler](https://golang.org/doc/install) is required to build this program. I recommend using Go version >= 1.11 as it will take care of downloading the proper versions all of the dependencies.

``` shellsession
$ go get bitbucket.org/uwaploe/sbf_filter
```

The binary will be installed in the `$GOPATH/bin` directory.

## Usage

``` shellsession
$ sbf_filter --help
Usage: sbf_filter [options] BLOCKNUM

Extract all BLOCKNUM records from an SBF data stream as newline-delimited
JSON. The data records are read from standard input. Only a small subset
of record types are currently supported.
  -in string
        Read SBF records from a file
  -list
        Output a list of all block numbers
  -max int
        Set the maximum number of records to output, 0=unlimited
```

## Examples

### List all records

``` shellsession
$ sbf_filter --list < pvt-20190130-18.sbf
1: 2019-01-30 18:54:42 +0000 UTC Id=4007 Size=96 crcOK=true
2: 2019-01-30 18:54:42 +0000 UTC Id=4001 Size=32 crcOK=true
3: 2019-01-30 18:59:42 +0000 UTC Id=4007 Size=96 crcOK=true
4: 2019-01-30 18:59:42 +0000 UTC Id=4001 Size=32 crcOK=true
```

### Extract the first 4007 record

``` shellsession
$ sbf_filter --max 1 4007 < pvt-20190130-18.sbf
{"t":"2019-01-30T18:54:42Z","mode":1,"error":0,"lat":0.8317404691449314,"lon":-2.134833489769472,"height":23.751235648259396,"und":-17.29379,"vn":0.0030557346,"ve":-0.0018424514,"vu":0.0056219893,"cog":-20000000000,"clk_bias":0.000011416281183218957,"clk_drift":0.00026645782,"time_sys":0,"datum":0,"nr_sv":18,"wa_corr":0,"ref_id":65535,"mean_corr_age":65535,"sig_info":2309,"alerts":1}
```

### Use jq to process output

In the example below, we use [jq](https://stedolan.github.io/jq/) to convert the latitude and longitude values from radian to degrees, calculate altitude from height and geoid undulation, and output as CSV:

``` shellsession
sbf_filter --in pvt-20190130-18.sbf 4007 | \
jq -M -r \
'{t: .t, lat: .lat, lon: .lon, h: .height, u: .und}|[.t, .lat*57.29578, .lon*57.29578, .h-.u]|@csv'
"2019-01-30T18:54:42Z",47.655218937224774,-122.31694996646392,41.0450256482594
"2019-01-30T18:59:42Z",47.65520510369971,-122.31694520331013,41.28720795659041
```
