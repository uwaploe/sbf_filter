// Extract all SBF records with a specified block number and output as
// newline delimited JSON.
package main

import (
	"encoding/binary"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"

	sbf "bitbucket.org/uwaploe/go-sbf"
)

var usage = `Usage: sbf_filter [options] BLOCKNUM

Extract all BLOCKNUM records from an SBF data stream as newline-delimited
JSON. The data records are read from standard input. Only a small subset
of record types are currently supported.
`
var (
	listOnly = flag.Bool("list", false, "Output a list of all block numbers")
	inFile   = flag.String("in", "", "Read SBF records from a file")
	maxRecs  = flag.Int("max", 0, "Set the maximum number of records to output, 0=unlimited")
)

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}
	flag.Parse()

	args := flag.Args()

	if !*listOnly && len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	var (
		file     io.Reader
		err      error
		blocknum int
	)

	if *inFile != "" {
		file, err = os.Open(*inFile)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		file = os.Stdin
	}

	if !*listOnly {
		blocknum, _ = strconv.Atoi(args[0])
	}

	scanner := sbf.NewScanner(file)
	matches := int(0)
	for i := 1; scanner.Scan(); i++ {
		b := scanner.Block()
		if *listOnly {
			fmt.Printf("%d: %s Id=%d Size=%d crcOK=%v\n", i,
				b.Time(),
				b.BlockNum(), b.Length, !b.CrcFailed())
		} else if int(b.BlockNum()) == blocknum {
			if b.CrcFailed() {
				log.Printf("CRC failure: index=%d Id=%d", i, b.BlockNum())
				continue
			}
			var v interface{}
			switch b.BlockNum() {
			case 4027:
				v, err = sbf.ReadMeasEpoch(b.Reader())
				if err != nil {
					log.Fatal(err)
				}
			case 4007:
				v = &sbf.PvtGeodetic{}
				err = binary.Read(b.Reader(), sbf.ByteOrder, v)
				if err != nil {
					log.Fatal(err)
				}
			case 4001:
				v = &sbf.DOP{}
				err = binary.Read(b.Reader(), sbf.ByteOrder, v)
				if err != nil {
					log.Fatal(err)
				}
			case 5891:
				v = &sbf.GpsNav{}
				err = binary.Read(b.Reader(), sbf.ByteOrder, v)
				if err != nil {
					log.Fatal(err)
				}
			case 5892:
				v = &sbf.GpsAlm{}
				err = binary.Read(b.Reader(), sbf.ByteOrder, v)
				if err != nil {
					log.Fatal(err)
				}
			case 5894:
				v = &sbf.GpsUtc{}
				err = binary.Read(b.Reader(), sbf.ByteOrder, v)
				if err != nil {
					log.Fatal(err)
				}
			case 5896:
				v = &sbf.GeoNav{}
				err = binary.Read(b.Reader(), sbf.ByteOrder, v)
				if err != nil {
					log.Fatal(err)
				}
			}

			out, err := json.Marshal(v)
			if err != nil {
				log.Fatal(err)
			}
			fmt.Println(string(out))
			matches++
			if *maxRecs > 0 && matches == *maxRecs {
				break
			}
		}
	}

	if err = scanner.Err(); err != nil {
		log.Fatal(err)
	}
}
